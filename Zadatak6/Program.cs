﻿using System;
using Zadatak5;

namespace Zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
            DVD dvd2 = new DVD("DVD Title", DVDType.MOVIE, 100);
            VHS vhs2 = new VHS("VHS Title", 30);
            Book book2 = new Book("Book Title", 15);
            RentVisitor visitor2 = new RentVisitor();
            Console.WriteLine("Cijene proizvoda: ");
            Console.WriteLine(dvd2.ToString());
            Console.WriteLine(vhs2.ToString());
            Console.WriteLine(book2.ToString());


            visitor2.Visit(dvd2);
            visitor2.Visit(vhs2);
            visitor2.Visit(book2);

            Console.WriteLine("Cijena iznajmljivanja: " + dvd2.Accept(visitor2));
            Console.WriteLine("Cijena iznajmljivanja: " + vhs2.Accept(visitor2));
            Console.WriteLine("Cijena iznajmljivanja: " + book2.Accept(visitor2));
        }
    }
}
