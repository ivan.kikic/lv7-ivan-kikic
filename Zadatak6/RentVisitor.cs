﻿using System;
using System.Collections.Generic;
using System.Text;
using Zadatak5;

namespace Zadatak6
{
    class RentVisitor : IVisitor
    {
        private const double Tax = 0.1;
        public double Visit(DVD DVDItem)
        {
            if (DVDItem.Type == DVDType.SOFTWARE)
            {
                return double.NaN;
            }
            else
            {
                return DVDItem.Price * (1 + Tax);
            }
        }
        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * (1 + Tax);
        }

        public double Visit(Book BookItem)
        {
            return BookItem.Price * (1 + Tax);
        }
    }
}
