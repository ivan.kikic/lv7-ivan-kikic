﻿using System;

namespace Zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array2 = { 1, 2, 3, 4, 5, 7, 8, 9, 10 };
            double[] array3 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            NumberSequence polje2 = new NumberSequence(array2);
            NumberSequence polje3 = new NumberSequence(array3);

            SearchStrategy search = new LinearSearch(6);

            Console.WriteLine("## array without number 6 ##");
            polje2.SetSearchStrategy(search);
            polje2.Search();
            Console.WriteLine("## array with number 6 ##");
            polje3.SetSearchStrategy(search);
            polje3.Search();
        }
    }
}
