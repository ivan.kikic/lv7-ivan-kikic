﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak2
{
    class LinearSearch : SearchStrategy
    {
        private double target;

        public LinearSearch(double target)
        {
            this.target = target;
        }
        public void Search(double[] array)
        {
            int cnt = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == target)
                {
                    cnt = 1;
                }
            }
            if (cnt == 1)
            {
                Console.WriteLine("Broj je uspjesno pronađen!");
            }
            else { Console.WriteLine("Broj nije pronađen!"); }
        }
    }
}
