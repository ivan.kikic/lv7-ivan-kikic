﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak5
{
    interface IItem
    {
        double Accept(IVisitor visitor);
    }
}
