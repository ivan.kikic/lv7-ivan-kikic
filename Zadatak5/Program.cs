﻿using System;

namespace Zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            DVD dvd = new DVD("DVD Title", DVDType.SOFTWARE, 100);
            VHS vhs = new VHS("VHS Title", 50);
            Book book = new Book("Book Title", 25);
            BuyVisitor visitor = new BuyVisitor();


            visitor.Visit(dvd);
            visitor.Visit(vhs);
            visitor.Visit(book);

            Console.WriteLine("Price with tax: " + dvd.Accept(visitor));
            Console.WriteLine("Price with tax: " + vhs.Accept(visitor));
            Console.WriteLine("Price with tax: " + book.Accept(visitor));

            Console.WriteLine(dvd.ToString());
            Console.WriteLine(vhs.ToString());
            Console.WriteLine(book.ToString());
        }
    }
}
