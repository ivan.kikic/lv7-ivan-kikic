﻿using System;

namespace Zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            Cart cart = new Cart();
            RentVisitor visitor3 = new RentVisitor();
            DVD dvd3 = new DVD("DVD Title", DVDType.MOVIE, 50);
            DVD dvd4 = new DVD("DVD Title2", DVDType.SOFTWARE, 40);
            VHS vhs3 = new VHS("VHS Title", 30);
            Book book3 = new Book("Book Title", 20);

            cart.AddToCart(dvd3);
            cart.AddToCart(vhs3);
            cart.AddToCart(book3);
            cart.AddToCart(dvd4);
            Console.WriteLine("Cijena iznajmljivanja svih elemenata iz kosare: ");
            Console.WriteLine(cart.Accept(visitor3));
        }
    }
}
