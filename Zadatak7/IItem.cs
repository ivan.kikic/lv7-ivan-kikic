﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak7
{
    interface IItem
    {
        double Accept(IVisitor visitor);
    }
}
