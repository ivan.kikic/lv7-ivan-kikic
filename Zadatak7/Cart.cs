﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak7
{
    class Cart
    {
        List<IItem> list = new List<IItem>();

        public void AddToCart(IItem item)
        {
            list.Add(item);
        }

        public void RemoveFromCart(IItem item)
        {
            list.Remove(item);
        }

        public double Accept(IVisitor visitor)
        {
            double rez = 0;
            foreach (IItem item in list)
            {
                rez += item.Accept(visitor);
            }
            return rez;
        }
    }
}
