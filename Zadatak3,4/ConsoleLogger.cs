﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak3
{
    class ConsoleLogger : Logger
    {
        public void Log(SimpleSystemDataProvider provider)
        {
            Console.WriteLine("CPU load: " + provider.CPULoad + "\nRAM Available: " + provider.AvailableRAM);
        }
    }
}
