﻿using System;

namespace Zadatak3
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider provider = new SystemDataProvider();

            FileLogger logg = new FileLogger("ivankikic.txt");
            ConsoleLogger log = new ConsoleLogger();
            provider.Attach(log);
            provider.Attach(logg);

            while(true)
            {
                provider.GetCPULoad();
                provider.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
