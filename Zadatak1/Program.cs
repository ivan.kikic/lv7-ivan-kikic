﻿using System;

namespace Zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 3, 2, 1, 5, 8, 10, 4, 9, 6, 7 };
            NumberSequence polje = new NumberSequence(array);
            Console.WriteLine("## unsorted Array ##");
            Console.Write(polje.ToString());
            Console.WriteLine();
            SortStrategy sort1 = new CombSort();
            SortStrategy sort2 = new BubbleSort();
            SortStrategy sort3 = new SequentialSort();
            Console.WriteLine("## CombSort ##");
            polje.SetSortStrategy(sort1);
            polje.Sort();
            Console.Write(polje.ToString());


            Console.WriteLine("## BubbleSort ##");
            polje.SetSortStrategy(sort2);
            polje.Sort();
            Console.Write(polje.ToString());

            Console.WriteLine("## SeqSort ##");
            polje.SetSortStrategy(sort3);
            polje.Sort();
            Console.Write(polje.ToString());
        }
    }
}
